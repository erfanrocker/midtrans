#!/bin/bash

# with minikubes
kubectl create deployment first-deployment --image=cluster/docker-http-server
kubectl get pods
kubectl expose deployment first-deployment --port=80 --type=NodePort
export PORT=$(kubectl get svc first-deployment -o go-template='{{range.spec.ports}}{{if .nodePort}}{{.nodePort}}{{"\n"}}{{end}}{{end}}')
echo "Accessing host01:$PORT"
curl host01:$PORT
minikube addons enable dashboard
kubectl apply -f /opt/kubernetes-dashboard.yaml
kubectl get pods -n kube-system  -w

# Deploying Local volume with yaml
kubectl create -f app/local-volumes.yaml
kubectl describe deployment local-volumes


# Deploying Wordpress with yaml
kubectl create -f app/wordpress-deployment.yaml
kubectl describe deployment webapp1

# Deployment service
kubectl create -f wp-service.yaml
kubectl get svc
kubectl describe svc webapp1-svc
curl host01:80

kubectl create -f app/wordpress-deployment.yaml

kubectl get deployment
kubectl get pods
curl host01:80
# Deploying Mysql with yaml
kubectl create -f app/mysql-deployment.yaml
kubectl describe deployment db

# Deployment service
kubectl create -f my-service.yaml
kubectl get svc
kubectl describe svc db-svc
curl host01:3306

kubectl create -f app/mysql-deployment.yaml

kubectl get deployment
kubectl get pods
curl host01:3306

